clear all
close all
clc
%-- LAB 01 --
%getSineSignal
%-- LAB 02 ---
%getSquareSignal
%getSawToothSignal
%getTriangleSignal
%-- LAB 03 --
%multSignal
%divSignal
%addSignal
%subSignal
%getMSE
%-- LAB 04 --
%getDecimation
%-- LAB 05 --
%doDFT
%doIDFT
%-- LAB 06 --
fs = 1024;%Hz
A=1;%amplituda
f=1;%Hz
fi=0;%rad
sim_time=1;%czas symulacji
t=0:1/1024:sim_time;%ilosc probek
accuracy1=7;
accuracy2=4;
ST = pt(fs);
[ x1 ] = ST.getSquareSignal(A,f,sim_time,fi,t,accuracy1);  
[ x2 ] = ST.getSquareSignal(A,f,sim_time,fi,t,accuracy2);  
[ dec ] = ST.getDecimation(x1,42);
[ te ] = ST.getDecimation(t,42);
figure('Name','Sinusoida');
hold on
plot(t,x1,'r');
plot(te,dec,'k');
hold off
grid on
title('A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');



