close all
clear all
clc
ST=pt();
c=1;
%zr�nicowanie kodu
s='A':'F';
%dlugosc kodu 
[l,Fs]=audioread('everything-is-fine.wav');
%xy = rgb2gray(imread('lennagrey.png'));
lr=l(:,1);
dlugoscKodu=50;
%%
for n=1:1:1
    %clc
    %msg=s(randi(length(s),dlugoscKodu,1)); % s=  zr�nicowanie alfabetu n= liczba znakow 
    %msg=lr(11:21)';
    msg=lr';
    msg=ST.doTransformToChar(msg);
    % Dopuszczalny s�ownik kodowy
    alphabet=ST.doAlphabet(msg)
    %clc
    %%
    % Szacowanie prawdopodobie�stw wyst�pienia znak�w ze s�ownika kodowego
    [ sortedAlphabet, sortedProbabilityDistribution ] = ST.getProb( msg, alphabet );

    % Rysowanie wykresu prawdopodobie�stwa wyst�pienia znak�w ze s�ownika kodowego
    ST.plotProb( sortedAlphabet, sortedProbabilityDistribution );
    %close all
    % Obliczanie drzewa kodowego Huffmana
    [ codeBook ] = ST.huffTree( sortedAlphabet, sortedProbabilityDistribution )

    % Kodowanie danych na podstawie obliczonego drzewa
    [ encodeMsg ] = ST.encodeHuff( codeBook, msg );
    % Dekodowanie danych na podstawie obliczonego drzewa
    [ msg2 ] = ST.decodeHuff( codeBook, encodeMsg )
    
    %-------------------------------------------%
    %%
    % Entropia �r�d�a
     H(c)  = ST.doEntropy( sortedProbabilityDistribution);
    % �rednia d�ugo�� kodu
     L(c)  = ST.doAverageCodeLength( codeBook, sortedProbabilityDistribution );

    % Efektywno�� kodowania
     E(c) = ST.doEfficiency( H, L );

     D(c)=dlugoscKodu;
     c=c+1;
end
%%
% figure
% hold on
% plot(H)
% plot(L)
% title('Zaleznosci Entropii oraz �redniej dlugosci kodu do ilosci danych');
% xlabel('Kolejne proby '), ylabel('warto�� H lub L ');
% hold off
% 
% figure
% hold on
% plot(E)
% title('Zaleznosci Efektywnosci od ilosci danych');
% xlabel('Kolejne proby'), ylabel('warto�� Efektywno�ci ');
% hold off
%mo�na zauwa�y� �e im wi�ksza dlugo�� kodu tym mniejsza efektywnosc
disp('Srednia efektywnosc dla  zmiennych w ilosci ')
disp(dlugoscKodu)
mean(E)
disp('Oraz roznorodnosci kodu wynoszacej  ')
disp(length(s))
%%
% �rednia z 10 prob

% dla 200 zmiennych i roznorodnosci 6  to jest jakies 98 efektywnosc
% dla 400 zmiennych i roznorodnosci 6  to jest jakies 98 efektywnosc
% dla 1000 zmiennych i roznorodnosci 6  to jest jakies 97.8 efektywnosc
% dla 10000 zmiennych i roznorodnosci 6  to jest jakies 97.2 efektywnos

% dla 200 zmiennych i roznorodnosci 26 to jest jakies 99 efektywnosc
% dla 400 zmiennych i roznorodnosci 26 to jest jakies 99 efektywnosc
% dla 1000 zmiennych i roznorodnosci 26 to jest jakies 99 efektywnosc
% dla 10000 zmiennych i roznorodnosci 6  to jest jakies 98,8 efektywnos

%%
%odp zad 2.3
%Wszystko zale�y od d�ugo�ci kodu i poj�cia s�owa du�o 
%bo np
%je�li na zakodowanie ka�dej kiltery  aflabetu potrzeba nam 4-5 biti�w ale
%raczej 5 to na zakodowanie 26 liter alfabetu bd nam potrzeba jakos 10
%bit�w  a przy r�norodno�ci bd potrzeba jednie lekko ponad 2 bit�w do tej
%operacji problem w tym �e je�li bd potrzebowali 2 bit�w ale danych bd np
%ju� 100 to tak s��bo op�acalne to jest xD
