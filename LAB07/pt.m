classdef pt < handle % -----> Deklaracja klasy pt <----
   %PT - Podstawy teleinformatyki
   % Gagatek Pawe� 41591
   properties %( Access = public )
      internal_fs; %zmienna wewn�trz klasy 
      audio_sample;
      image_sample;
      txt_file;
   end
       %%   
       methods( Access = public ) % -----> merody w klasie <----
       %%
       function [ obj ] = pt(  ) % / pt ( fs )
          obj.internal_fs = 0 ;
          audio_sample="";
          image_sample="";
          txt_file="";
%         konstruktor ( sprawdza dane wejsciowe )
%          if nargin > 0 % liczba argumen�w wejsciowych 
%             if isnumeric(fs)
%                obj.internal_fs = fs;
%             else
%                error('Bad input data')
%             end
%          end
       end
       %---------------------------------------%
       %%
       function [ x ] = getSquareSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal prostokatny
       w=2*pi*f;
       x = zeros(1,length(t));
       for x2=1:2:accuracy1
       x=x+((4*A)/pi)*((sin(x2*w*t))/x2); 
       end
       end
       %---------------------------------------%
       %%
       function [ x ] = getSawToothSignal(obj,A,f,sim_time,fi,t,accuracy2) % Metoda zwracaj�ca sygnalpi�okszta�tny
       w=2*pi*f;
       x = zeros(1,length(t));
       for x2=1:1:accuracy2;
          if mod(x2,2)==1
              x=x+((2*A)/pi)*((sin(x2*w*t))/x2); 
          else
              x=x-((2*A)/pi)*((sin(x2*w*t))/x2);
          end
       end
       end
       %---------------------------------------%       
       %%
       function [ x ] = getTriangleSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal trojkatny
       w=2*pi*f;
       dod=1;
       x = zeros(1,length(t));
       for x2=1:2:accuracy1;
          if dod==1
              dod=0;
              x=x+((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2)); 
          else
              dod=1;
              x=x-((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2));
          end
       end
       end
       %---------------------------------------%
       %%       
       function [ x ] = getSineSignal(obj,A,f,sim_time,fi,t) % Metoda zwracaj�ca pr�bk� w czasie
       x=A*sin(2*pi*t*f+fi);
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = addSignal(obj,x1,x2) % Metoda zwracaj�ca sume pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1+x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1+x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1+x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = subSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1-x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1-x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1-x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = multSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1.*x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1.*x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1.*x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = divSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1./x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1./x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1./x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%      
       function [ MSE ] = getMSE(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
       MSE=immse(x1,x2);    
       end
       %---------------------------------------%
       %%      
       function [ x1d ] = getDecimation(obj,x1,M)
       N=max(size(x1));
       x1d=x1(1:M:N)
       end
       %---------------------------------------%
       %%      
       function [ A ] = doDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
              A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(-k*n);
              %A(k)=A(k)+x(n)*exp((-i)*2*pi*k*n/N);
           end
       end
       end
       %---------------------------------------%
       %%    
       function [ A ] = doIDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
               %A(k)=A(k)+x(n)*exp(i*2*pi*k*n/N);
               A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(k*n);
           end
       end
       A=A./N;
       end
       %---------------------------------------%
       %%      
       function [ y ] = myFFT(obj,vect)
        leng=max(size(vect));
        if leng==1
             y=vect;
        else
            %even(1:1:leng/2)=vect(1:2:leng-1);
            %odd(1:1:leng/2)=vect(2:2:leng); 
            y=[obj.moj(vect(1:2:leng-1)) obj.moj(vect(2:2:leng))];
            for n=1:leng/2
                  t=y(n);
                  y(n)=t+exp(2*pi*i*n/leng)*y(n+leng/2);
                  y(n+leng/2)=t-exp(2*pi*i*n/leng)*y(n+leng/2);
            end
        end
       end
       %---------------------------------------%
       %%    
       function [ G ] = doDCT(obj,vect)
       N=max(size(vect));
       G=zeros(1,N);
       for m=1:N
            G(1)=G(1)+vect(m);
        end
        G(1)=G(1)/sqrt(N);
        for k=2:N
            for m=1:N
                G(k)=G(k)+vect(m)*cos((pi*k*(2*m)+1)/(2*N));                
            end
            G(k)=G(k)*sqrt(2/N);
        end
       end
       %---------------------------------------%
       %%    
       function [ G ] = doIDCT(obj,vect)
       N=max(size(vect));
       G=zeros(1,N);
       for k=1:N
            for m=1:N
                G(k)=G(k)+vect(m)*cos((pi*k*(2*m)+1)/(2*N));                
            end
            G(k)=G(k)+1/sqrt(N)*vect(1)+sqrt(2/N);
       end
       end
       %---------------------------------------%
       %%    
       function [ obj ] = loadAudio(obj,filename)
       obj.audio_sample=audioread(filename);
       end
       %---------------------------------------%
       %%     
       function [ obj ] = loadImage(obj,filename)
       obj.image_sample=imread(filename);
       end
       %---------------------------------------%
       %%    
       function [ obj ] = loadTxt(obj,filename)
       obj.txt_file=importdata(filename);  
       end
       %---------------------------------------%
       %%     
       function [  ] = test(obj)
       figure,plot(obj.audio_sample)
       figure,imshow(obj.image_sample)
       disp(obj.txt_file)
       end
       %---------------------------------------%
       %%    
       function [ H ] = doEntropy(obj , sortedProbabilityDistribution );
       H=sortedProbabilityDistribution.*log2(sortedProbabilityDistribution)*(-1);
       H=sum(H);
       end
       %---------------------------------------%
       %%     
       function [ L ] = doAverageCodeLength(obj, codeBook, sortedProbabilityDistribution )
       F1 = cellfun(@length, codeBook(1:end,2)); % dlugosc bit�w na kazda litere
       F2=zeros(length(F1),1); % zamiana index�w
       
       for x=1:length(F1)
            F2(x)=F1(length(F1)-x+1);
       end
       
       L=sum(sortedProbabilityDistribution.*F2);
       end
       %---------------------------------------%
       %%     
       function [ E ] = doEfficiency( obj , H, L );
       E=H/L*100;
       end
       %---------------------------------------%
       %%
       function [ alphabet ] = doAlphabet( obj , msg )
       alphabet='';
       s=['A':'Z' 'a':'z'  '!':'?' '{':'}' ' `~']
       % Dopuszczalny s�ownik kodowy
       if isnumeric(msg)==1
           msg=sprintf('%g',msg);
           %The more compact of %e or %f, with no trailing zeros     
           %(Use a precision operator to specify the number of significant digits.)
           %Format data into string
           %g- po prostu format liczb
       end
       b=1;
       for x3=s
            a=strfind(msg,x3);
            if length(a)>0
                alphabet(b)=x3;
                b=b+1;   
            end
       end
       end
       %---------------------------------------%
       %%
       function [ msg] = doTransformToChar ( obj , msg )
       if isnumeric(msg)==1
           msg=sprintf('%g',msg);
           %The more compact of %e or %f, with no trailing zeros     
           %(Use a precision operator to specify the number of significant digits.)
           %Format data into string
           %g- po prostu format liczb
       end
       end
       %---------------------------------------%
       %%
       function [ msg] = getRandomMsg ( obj , s , dlugoscKodu)
       % s=['A':'Z' 'a':'z'  '!':'?' '{':'}' ' `~']
       msg=s(randi(length(s),dlugoscKodu,1));     
       end
       %% Metody  do kompresji huffmana -- do 562 lini 
       function [ codeBook ] = huffTree(obj, sortedAlphabet, sortedProbabilityDistribution )



       % Rotate vector of probability distribution
       sortedProbabilityDistribution=sortedProbabilityDistribution';

       %% Console Output - Initial Characters and Their Respective Probabilties
       % Display character vs. probability

       disp('Character Probability:');
       for i = 1:length(sortedProbabilityDistribution)
            display(strcat(sortedAlphabet(i),' -->  ',num2str(sortedProbabilityDistribution(i))));
       end

       %% Initialize The Encoding Array
       % A cell array is used to hold the groups of symbols for encoding. The process of huffman encoding combines characters into larger symbols, and a cell is the ideal type of container for managing this type of data in MATLAB. Initialize a cell array (where each cell is a symbol)

       for i = 1:length(sortedAlphabet)
            sorted_str{i} = sortedAlphabet(i);
       end

       % Save initial set of symbols and probabilities for later use
       init_str = sorted_str;
       init_prob = sortedProbabilityDistribution;

       %% Huffman Encoding Process
       % Iteratively sort and combine symbols based on their probabilities. Each iteration sorts the available symbols and then takes the two symbols with the "worst" probabilties and combines them into a single symbol for the next iteration. The loop halts when only a single symbol remains.

       sorted_prob = sortedProbabilityDistribution;
       rear = 1;
       while (length(sorted_prob) > 1)
            % Sort probs
            [sorted_prob,indeces] = sort(sorted_prob,'ascend');
            % Sort string based on indeces
            sorted_str = sorted_str(indeces);

            % Create new symbol
            new_node = strcat(sorted_str(1),sorted_str(2));
            new_prob = sum(sorted_prob(1:2));



            % Dequeue used symbols from "old" queue
            sorted_str =  sorted_str(3:length(sorted_str));
            sorted_prob = sorted_prob(3:length(sorted_prob));


            % Add new symbol back to "old" queue

            sorted_str = [sorted_str, new_node];
            sorted_prob = [sorted_prob, new_prob];

            % Add new symbol to "new" queue
            newq_str(rear) = new_node;
            newq_prob(rear) = new_prob;
            rear = rear + 1;
       end

       %% Form Huffman Tree Data
       % Get all elements for the tree (symbols and probabilities) by concatenating the original set of symbols with the new combined symbols and probabilities

       tree = [newq_str,init_str];
       tree_prob = [newq_prob, init_prob];

       % Sort all tree elements
       [sorted_tree_prob,indeces] = sort(tree_prob,'descend');
       sorted_tree = tree(indeces);

       %% Calculate Tree Parameters
       % Calculate parent relationships for all tree elements. This will allow the treeplot command to realize the correct tree structure.

       parent(1) = 0;
       for i = 2:length(sorted_tree)
           % Extract my symbol
           me = sorted_tree{i};

           % Find my parent's symbol (search until shortest match is found)
           count = 1;
           parent_maybe = sorted_tree{i-count};
           diff = strfind(parent_maybe,me);
           while (isempty(diff))
                count = count + 1;
                parent_maybe = sorted_tree{i-count};
                diff = strfind(parent_maybe,me);
           end
           parent(i) = i - count;
       end

       figure
       treeplot(parent);
       title(strcat('Huffman Coding Tree - "',sortedAlphabet,'"'));

       %% Console Output - Tree Symbols and Their Probabilities
       % Print out tree in text form
       display(sorted_tree)
       display(sorted_tree_prob)

       %% Tree Parameter Extraction
       % Extract binary tree parameters. These parameters include the (x,y) coordinates of each node so that we can accurately place label on the tree.

       [xs,ys,h,s] = treelayout(parent);

       %% Label Tree Nodes
       % Place labels on each tree node

       text(xs,ys,sorted_tree);

       %% Label Tree Edges
       % Put weights on the tree. The slope of each edge indicates whether it is a left-branch or a right-branch. Left-branches have positive slope and are labelled with a '1', while right-branches have negative slope and are labelled with a '0'. The labels for the weights are placed at the midpoint of each edge. Calculate mid-points for each node

       for i = 2:length(sorted_tree)
            % Get my coordinate
            my_x = xs(i);
            my_y = ys(i);

            % Get parent coordinate
            parent_x = xs(parent(i));
            parent_y = ys(parent(i));

            % Calculate weight coordinate (midpoint)
            mid_x = (my_x + parent_x)/2;
            mid_y = (my_y + parent_y)/2;

            % Calculate weight (positive slope = 1, negative = 0)
            slope  = (parent_y - my_y)/(parent_x - my_x);
            if (slope > 0)
                weight(i) = 1;
            else
                weight(i) = 0;
            end
            text(mid_x,mid_y,num2str(weight(i)));
       end

       %% Huffman Codebook Calculation
       % A huffman codebook can be calculated by traversing the huffman tree while recording the weights encountered while travelling to each node. Calculate codebook

       for i = 1:length(sorted_tree)
           % Initialize code
           code{i} = '';

           % Loop until root is found
           index = i;
           p = parent(index);
           while(p ~= 0)
                % Turn weight into code symbol
                w = num2str(weight(index));

                % Concatenate code symbol
                code{i} = strcat(w,code{i});

                % Continue towards root
                index = parent(index);
                p = parent(index);
           end
       end

       %% Display raw code book with prefixes
        codeBook = [sorted_tree', code']

       %% Rewind code book and clear prefixes
       rear=1;
       codeBookNonPrefix=cell(1,2);
       for i = 1:length(sorted_tree)
            if length(codeBook{i,1})==1
                codeBookNonPrefix{rear,1}=codeBook{i,1};
                codeBookNonPrefix{rear,2}=codeBook{i,2};
                rear=rear+1;
            end
       end
       codeBook = codeBookNonPrefix;
       end
       %---------------------------------------%
       %%
       function [ encodeMsg ] = encodeHuff( obj , codeBook, msg )
       encodeMsg='';
       for n=1:length(msg) % leci po kazdym znaku w code booku
           tmpIdx = strfind(codeBook(1:end,1), msg(n)); % temp inx = znajdz stringa w codebooku od msg (n)
           Idx = find(not(cellfun('isempty', tmpIdx)))'; % znajduje index na ktorym znaleciono ta wartosc
           encodeMsg = [encodeMsg, [codeBook{Idx,2}]]; % wiadomosc = waidomo�� + warto�� codebooka od znalezionych indexow
       end
       end
       %---------------------------------------%
       %%
       function [sortedAlphabet,sortedProbabilityDistribution] = getProb( obj, data, alphabet )
       N=max(size(data));
       sortedProbabilityDistribution=zeros(max(size(alphabet)),1);
       i=1;
       for c=alphabet
           sortedProbabilityDistribution(i)=length(find(data==c))/N;
           i=i+1;
       end
       [sortedProbabilityDistribution,idx] = sort(sortedProbabilityDistribution,'ascend');
       sortedAlphabet=alphabet(idx);
       end
       %---------------------------------------%
       %% 
       function [ msg ] = decodeHuff( obj , codeBook, encodeMsg )
       mL=0;
       for i=1:size(codeBook,1)
        mtL=length([codeBook{i,2}]);
        if mL < mtL
        mL = mtL;
        end
       end
       codeBook = sortrows(codeBook,[2]);
    
       msg='';
       N=max(size(encodeMsg));
       n=1;
       for l=1:N
        tmpCB={};
        rear=1;
        newTmpCB=codeBook;
        for ml=1:mL;
            for i=1:size(newTmpCB,1)
                if N>=(n+ml-1)
                    nml1=n+ml-1;
                    b=encodeMsg(nml1);
                    if([newTmpCB{i,2}(ml)]==b)
                        tmpCB{rear,1}=newTmpCB{i,1};
                        tmpCB{rear,2}=newTmpCB{i,2};
                        rear=rear+1;
                    end
                end
            end
        if rear==2
            msg=[msg,[tmpCB{1,1}]];
            n=n+ml;
            break;
        end
        newTmpCB=tmpCB;
        tmpCB=[];
        rear=1;
       end
       if n>N % or n>=N :)
        break;
       end
       end
       end
       %---------------------------------------%
       %%
       function [] = plotProb( obj , sortedAlphabet, sortedProbabilityDistribution )
       labels=cell(size(sortedAlphabet));
       for i=1:max(size(sortedAlphabet))
        labels{i}=[sortedAlphabet(i)];
       end
       figure;
       bar(sortedProbabilityDistribution);
       set(gca,'xticklabel',labels);
       title('Probability of alphabet');
       xlabel('Character');
       ylabel('Probability');
       end
       %---------------------------------------%
       %%
       end    % -----> merody w klasie <----
   
end % -----> Koniec klasy <----