clear all
close all
clc
%-- LAB 01 --
%getSineSignal
%-- LAB 02 ---
%getSquareSignal
%getSawToothSignal
%getTriangleSignal
%-- LAB 03 --
%multSignal
%divSignal
%addSignal
%subSignal
%getMSE
%-- LAB 04 --
%getDecimation
%-- LAB 05 --
%doDFT
%doIDFT
%myFFT
%-- LAB 06 --
fs = 1024;%Hz
A=1;%amplituda
f=10;%Hz
fi=0;%rad
sim_time=1;%czas symulacji
t=0:1/1024:sim_time;%ilosc probek
accuracy1=4;
accuracy1=accuracy1*2-1;
ST = pt();
[ x ] = ST.getSquareSignal(A,f,sim_time,fi,t,accuracy1);
[ x3 ] = ST.doDFT(x);
%---- czy parzysta ------
leng=max(size(x));
a=mod(leng,2);
if(a==0)
else
     a=leng-1;
     x2=x(1:1:a);
     t2=t(1:1:a);
end
%------------------------
vect=[1:1:leng-1]
N=size(vect);
idx=[dec2bin(0:N(2)-1)];
idx2=bin2dec(fliplr(idx))+1;
%------------------------
tic
[ p1 ] = fft(x2);
toc
tic
[ p2 ] = ST.myFFT(x2);
toc
tic
[ p3 ] = ST.doDFT(x2);
toc
subplot(3,1,1);
plot(t2,abs(p1),'r');
grid on
title('FFT orginal A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');
%------------------------------%
subplot(3,1,2);
plot(t2,abs(p2),'r')
grid on
title('FFT moje A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');
%------------------------------%
subplot(3,1,3);
plot(t,abs(x3),'b')
grid on
title('Square (H=4) A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');



