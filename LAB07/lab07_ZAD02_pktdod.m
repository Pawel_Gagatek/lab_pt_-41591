close all
clear all
clc
ST=pt();
c=1;

%zr�nicowanie kodu
s='A':'F';
%for n=10:100:10010
n=42;
    % Wiadomo�� do przes�ania 
    msg=s(randi(length(s),n,1)); % s= alfabet n= liczba znakow 
    % Dopuszczalny s�ownik kodowy
    b=1;
    for x3='A':'F'
        a=strfind(msg,x3);
        if length(a)>0
        alphabet(b)=x3;
        b=b+1;   
        end
    end
    clc
    % Szacowanie prawdopodobie�stw wyst�pienia znak�w ze s�ownika kodowego
    [ sortedAlphabet, sortedProbabilityDistribution ] = getProb( msg, alphabet );
     clc
    % Rysowanie wykresu prawdopodobie�stwa wyst�pienia znak�w ze s�ownika kodowego
    plotProb( sortedAlphabet, sortedProbabilityDistribution );
    close all
    clc
    % Obliczanie drzewa kodowego Huffmana
    [ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )
    close all
    clc
    % Kodowanie danych na podstawie obliczonego drzewa
    [ encodeMsg ] = encodeHuff( codeBook, msg );
    close all
    clc
    % Dekodowanie danych na podstawie obliczonego drzewa
    [ msg2 ] = decodeHuff( codeBook, encodeMsg )
    close all
    clc
    
    %-------------------------------------------%
    
    % Entropia �r�d�a
     H(c)  = ST.doEntropy( sortedProbabilityDistribution);

    % �rednia d�ugo�� kodu
     L(c)  = ST.doAverageCodeLength( codeBook, sortedProbabilityDistribution );

    % Efektywno�� kodowania
     E(c) = ST.doEfficiency( H, L );

     D(c)=n;
     c=c+1;
%end

figure
hold on
plot(D,H)
plot(D,L)
title('Zaleznosci Entropii oraz �redniej dlugosci kodu do ilosci danych');
xlabel('Ilo�� danych'), ylabel('warto�� H lub L ');
hold off

figure
hold on
plot(D,E)
title('Zaleznosci Efektywnosci od ilosci danych');
xlabel('Ilo�� danych'), ylabel('warto�� Efektywno�ci ');
hold off
%mo�na zauwa�y� �e im wi�ksza dlugo�� kodu tym mniejsza efektywnosc
