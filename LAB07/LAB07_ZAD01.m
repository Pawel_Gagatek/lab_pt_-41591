close all
clear all
clc
ST=pt();
% Wiadomo�� do przes�ania 
msg='AAAABBAAAABBAAAABBAAAADDCCDDCCDDDDCEEFFEEF';

% Dopuszczalny s�ownik kodowy
alphabet=['A','D','B','C','E','F'];

% Szacowanie prawdopodobie�stw wyst�pienia znak�w ze s�ownika kodowego
disp('Pi');
[ sortedAlphabet, sortedProbabilityDistribution ] = getProb( msg, alphabet );

% Rysowanie wykresu prawdopodobie�stwa wyst�pienia znak�w ze s�ownika kodowego
plotProb( sortedAlphabet, sortedProbabilityDistribution );

% Obliczanie drzewa kodowego Huffmana
[ codeBook ] = huffTree( sortedAlphabet, sortedProbabilityDistribution )

% Kodowanie danych na podstawie obliczonego drzewa
[ encodeMsg ] = encodeHuff( codeBook, msg );

% Dekodowanie danych na podstawie obliczonego drzewa
[ msg2 ] = decodeHuff( codeBook, encodeMsg )
%-------------------------------------------%


% pkt 8 kod huffmana bierze pod uwage cz�stotliwo�� wyst�powania zmiennej w
% Danym tekscie natomiast kod morse nie bierze pod uwag� przesy�anej
% informacji a og�ln� cz�stotliwo�� wyst�powania danej zmiennej w zwi�zku z
% czym nie jest optymalny dla ka�dej wiadomo�ci np takiej kt�ra zawiera 
% rzadko wystepuj�ce litery , dodatkowo nie ma potrzeby  kodowania slownika
% przy korzystaniu z kodu morsa. 

%-------------------------------------------%
% Poni�ej funkcje do samodzielnej implem�acji:

% Entropia �r�d�a
[H] = ST.doEntropy( sortedProbabilityDistribution);

% �rednia d�ugo�� kodu
[ L ] = ST.doAverageCodeLength( codeBook, sortedProbabilityDistribution );

% Efektywno�� kodowania
[ E ] = ST.doEfficiency( H, L );
%---------------------------------- Dane tabela
for x=1:length(alphabet)
     Liczebnosc(x)=count(msg,alphabet(x));
end

Symbole=codeBook(1:end,1);
Liczebnosc=Liczebnosc';
IloscBitow=cellfun(@length, codeBook(1:end,2));
KsiazkaZnakow=codeBook(1:end,2);
F1=sortedProbabilityDistribution;
F2=zeros(length(F1),1); % zamiana index�w
     for x=1:length(F1)
        F2(x)=F1(length(F1)-x+1);
     end
PrawdoPodobienstwo=F2;
Entropia=[H zeros(1,length(alphabet)-1)]';
SredniaDlugosc=[L zeros(1,length(alphabet)-1)]';
Efektywnosc=[E zeros(1,length(alphabet)-1)]';
% Dla ASCI ( 8b na zmienna )
FormaNiezakodowana=length(msg)*8;
FormaNiezakodowana=[FormaNiezakodowana zeros(1,length(alphabet)-1)]';
[ L ] = ST.doAverageCodeLength( codeBook, sortedProbabilityDistribution );
FormaZakodowana=sum(L*length(msg));
FormaZakodowana=[FormaZakodowana zeros(1,length(alphabet)-1)]';

%Entropia � �rednia ilo�� informacji, przypadaj�ca na pojedyncz� wiadomo�� 
%ze �r�d�a informacji. Innymi s�owy jest to �rednia wa�ona ilo�ci informacji niesionej przez pojedyncz� 
%wiadomo��, gdzie wagami s� prawdopodobie�stwa nadania poszczeg�lnych wiadomo�ci.
%----------------------------------
T=table(Symbole,Liczebnosc,KsiazkaZnakow,PrawdoPodobienstwo,IloscBitow,Entropia,SredniaDlugosc,Efektywnosc,FormaZakodowana,FormaNiezakodowana)

