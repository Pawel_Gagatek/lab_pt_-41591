clear all
close all
clc
%-- LAB 01 --
%getSineSignal
%-- LAB 02 ---
%getSquareSignal
%getSawToothSignal
%getTriangleSignal
%-- LAB 03 --
%multSignal
%divSignal
%addSignal
%subSignal
%getMSE
%-- LAB 04 --
%getDecimation
%-- LAB 05 --
%doDFT
%doIDFT
%myFFT
%-- LAB 06 --
fs = 1024;%Hz
A=1;%amplituda
f=10;%Hz
fi=0;%rad
sim_time=1;%czas symulacji
t=0:1/1024:sim_time;%ilosc probek
accuracy1=4;
accuracy1=accuracy1*2-1;
ST = pt();
[ x ] = ST.getSquareSignal(A,f,sim_time,fi,t,accuracy1);
[ p1 ] = ST.doDCT(x);
l=max(size(p1));
p1(150:1:l)=0;
[ p2 ] = ST.doIDCT(p1);
subplot(3,1,1);
plot(t,p1,'r');
grid on
title('FFT orginal A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');
%------------------------------%
subplot(3,1,2);
plot(t,p2,'r')
grid on
title('FFT moje A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');
%------------------------------%
subplot(3,1,3);
plot(t,x,'b')
grid on
title('Square (H=4) A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');



