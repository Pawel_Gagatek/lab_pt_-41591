clear all
close all
clc

suma=0;
ST=pt();

%macierz kwantyzacji dla wsp�czynnik�w luminancji
Qy = ...
   [ 16  11  10  11  24  40  51  61
     12  12  14  19  26  58  60  55
     14  13  16  24  40  57  69  56
     14  17  22  29  51  87  80  62
     18  22  37  56  68 109 103  77
     24  35  55  64  81 104 113  92
     49  64  78  87 103 121 120 101
     72  92  95  98 112 100 103  99];

%macierz kwantyzacji dla wsp�czynnik�w chrominancji
Quv = ...
   [ 17  18  24  47  99  99  99  99
     18  21  26  66  99  99  99  99
     24  26  56  99  99  99  99  99
     47  66  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99];

load('dana.mat')
c=dana(1,1).c;
 imgBMP2=uint8(zeros(512,512));
  for x=1:1:64 
    a1=x*8;
    a2=a1-7;
    for  y=1:1:64 
             b1=y*8;
             b2=b1-7;
             
             sizeJpeg = length(dana(x,y).Y) + length(dana(x,y).U) + length(dana(x,y).V)+length(dana(x,y).c);
             suma=suma+ 100 * (sizeJpeg /(3*8*8));
             
             rV=dana(y,x).V;
             rY=dana(y,x).Y;
             rU=dana(y,x).U;
%% Dekodowanie
%IRLE 
[ uV2 ] = ST.IRLE64( rV );
[ uY2 ] = ST.IRLE64( rY );
[ uU2 ] = ST.IRLE64( rU );

% unsign2sign
 [ zY2 ] = ST.unsign2sign( uY2 );
 [ zU2 ] = ST.unsign2sign( uU2 );
 [ zV2 ] = ST.unsign2sign( uV2 );

%odwrotny zigzag
 [ qY2 ] = ST.zYUV2qYUV( zY2);
 [ qU2 ] = ST.zYUV2qYUV( zU2 );
 [ qV2 ] = ST.zYUV2qYUV( zV2 );

% dekwantyzacja
[ fY2, fU2, fV2 ] = ST.qYUV2fYUV ( qY2, qU2, qV2, Qy, Quv, c );

% idct2
[ Y2 ] = ST.YUV2IFDCT( fY2 );   
[ U2 ] = ST.YUV2IFDCT( fU2 ); 
[ V2 ] = ST.YUV2IFDCT( fV2 );
 
% deMapValuie
[ Y2, U2, V2 ] = ST.deMapValue( Y2, U2, V2 );

% YUV 2 RGB
[ R2 , G2 , B2 ] = ST.yuv2rgb( Y2, U2, V2 );

%RGB to BMP
[ excerpt2(a2:a1,b2:b1,:) ] = ST.getExcerpt(  R2 , G2 , B2 );

%               imgBMP2(a2:a1,b2:b1,1)=excerpt2(a2:a1,b2:b1,1);
%               imgBMP2(a2:a1,b2:b1,2)=excerpt2(a2:a1,b2:b1,2);
%               imgBMP2(a2:a1,b2:b1,3)=excerpt2(a2:a1,b2:b1,3);
                imgBMP2(a2:a1,b2:b1,1)=R2;
                imgBMP2(a2:a1,b2:b1,2)=G2;
                imgBMP2(a2:a1,b2:b1,3)=B2;
  end
  end
imgBMP = imread('lennacolor24b.bmp');
srednia=suma/(64^2);
subplot(1,2,1);
imshow(imgBMP)
title(['Oryginal photo'])
subplot(1,2,2);
imshow(imgBMP2)
title(['This figure used c = ', num2str(c),' stopien kompresji = ',num2str(srednia),' % '])
