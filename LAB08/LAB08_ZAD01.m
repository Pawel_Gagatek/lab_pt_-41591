%BMP->JPEG

clear all
close all
clc

c=1;
suma=0;
ST=pt();
%macierz kwantyzacji dla wsp�czynnik�w luminancji
Qy = ...
   [ 16  11  10  11  24  40  51  61
     12  12  14  19  26  58  60  55
     14  13  16  24  40  57  69  56
     14  17  22  29  51  87  80  62
     18  22  37  56  68 109 103  77
     24  35  55  64  81 104 113  92
     49  64  78  87 103 121 120 101
     72  92  95  98 112 100 103  99];

%macierz kwantyzacji dla wsp�czynnik�w chrominancji
Quv = ...
   [ 17  18  24  47  99  99  99  99
     18  21  26  66  99  99  99  99
     24  26  56  99  99  99  99  99
     47  66  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99
     99  99  99  99  99  99  99  99];

% odczyt obrazu
imgBMP = imread('lennacolor24b.bmp');
%%
%wycinek obrazu 8x8 pikseli RGB
%imshow(imgBMP);

 for( x=1:1:64 )
    a1=x*8;
    a2=a1-7;
    for ( y=1:1:64 )
             b1=y*8;
             b2=b1-7;
             excerpt = imgBMP(a2:a1,b2:b1,:);
             %excerpt = imgBMP(1:8,1:8,:);
            
%TU ROZBUDUJ SKRYPT ABY WYCINA� W P�TLI
%AUTOMATYCZNIE KAWA�EK PO KAWA�KU FRAGMENTY 8x8 PIKSELI 
%Z OBRAZU 

%% Kodowanie
%%
%wydobycie sk�adowych koloru
[R, G, B] = ST.getRGB(excerpt);
% odwrotne
%[ excerpt2 ] = ST.getExcerpt(  R , G , B )

%%
% zmiana przestrzeni barw RGB na luminancje i chrominancje
[Y, U, V] = ST.rgb2yuv( R, G, B );
%odwrotne
%[ R , G , B ] = ST.yuv2rgb( Y, U, V );
%%
% konwersja przedzia��w warto�ci z [0..255] na [-128..127]
 [ Y, U, V ] = ST.mapValue( Y, U, V );
% odwrotne
%[ Y, U, V ] = ST.deMapValue( Y, U, V );
%%
% dyskretna transformacja kosinusowa
  [ fY ] = ST.YUV2FDCT( Y );
  [ fU ] = ST.YUV2FDCT( U );
  [ fV ] = ST.YUV2FDCT( V );
  %odwrotne  
%   [ Y  ] = ST.YUV2IFDCT( fY );   
%   [ U  ] = ST.YUV2IFDCT( fU ); 
%   [ V  ] = ST.YUV2IFDCT( fV );

%%  kwantyzacja
% C>1 zwi�kszenie kompresji ( pogorszeni )
% C<1 & C>0 zminiejszenie kompresji ( polepszenie )

% %kwantyzacja wraz z kompresj� uzale�nion� od parametru "c"

[ qY, qU, qV ] = ST.fYUV2qYUV( fY, fU, fV, Qy, Quv, c );
%odwrotne
%[ fY, fU, fV ] = ST.qYUV2fYUV ( qY, qU, qV, Qy, Quv, c );

%% 
% %reorganizacja macierzy na wektory
[ zY ] = ST.qYUV2zYUV( qY);
[ zU ] = ST.qYUV2zYUV( qU );
[ zV ] = ST.qYUV2zYUV( qV );
%odwrotne
% [ qY ] = ST.zYUV2qYUV( zY);
% [ qU ] = ST.zYUV2qYUV( zU );
% [ qV ] = ST.zYUV2qYUV( zV );

%%
% % %konwersja zapisu liczb ujemnych w postaci dodatnich nieparzystych\

[ uY ] = ST.sign2unsign( zY );
[ uU ] = ST.sign2unsign( zU );
[ uV ] = ST.sign2unsign( zV );
%odwrotne
% [ zY ] = ST.unsign2sign( uY );
% [ zU ] = ST.unsign2sign( uU );
% [ zV ] = ST.unsign2sign( uV );

%%
% %kodowanie d�ugo�ci serii (RLE)

[ rV ] = ST.RLE64( uV );
[ rY ] = ST.RLE64( uY );
[ rU ] = ST.RLE64( uU );
%odwrotne
%  [ uV ] = ST.IRLE64( rV );
%  [ uY ] = ST.IRLE64( rY );
%  [ uU ] = ST.IRLE64( rU );

%% strukturka
%utworzenie roboczej struktury na dane jpeg
jpeg = struct('Y', rY, 'U', rU, 'V', rV, 'c', c);
%jpeg = struct('Y', rY, 'U', rU, 'V', rV);

%fprintf('Rozmiar: %dB (%f%% pierwotnego rozmiaru)\n\n', sizeJpeg, 100 * (sizeJpeg /(3*8*8)));
%disp(jpeg)
%
%zapisanie do struktury macierzy
dana(y,x)=jpeg;
end
end
save('dana.mat','dana');

