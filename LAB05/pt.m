classdef pt % -----> Deklaracja klasy pt <----
   %PT - Podstawy teleinformatyki
   % Gagatek Pawe� 41591
   properties
      internal_fs %zmienna wewn�trz klasy 
   end
   
   methods  % -----> merody w klasie <----
      function [ obj ] = pt( fs )
         %konstruktor ( sprawdza dane wejsciowe )
         if nargin > 0 % liczba argumen�w wejsciowych 
            if isnumeric(fs)
               obj.internal_fs = fs;
            else
               error('Bad input data')
            end
         end
      end
       %---------------------------------------%
      function [ x ] = getSquareSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal prostokatny
      w=2*pi*f;
      x = zeros(1,length(t));
      for x2=1:2:accuracy1
      x=x+((4*A)/pi)*((sin(x2*w*t))/x2); 
      end
      end
      %---------------------------------------%
      function [ x ] = getSawToothSignal(obj,A,f,sim_time,fi,t,accuracy2) % Metoda zwracaj�ca sygnalpi�okszta�tny
      w=2*pi*f;
      x = zeros(1,length(t));
      for x2=1:1:accuracy2;
          if mod(x2,2)==1
              x=x+((2*A)/pi)*((sin(x2*w*t))/x2); 
          else
              x=x-((2*A)/pi)*((sin(x2*w*t))/x2);
          end
      end
      end
       %---------------------------------------%       
       function [ x ] = getTriangleSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal trojkatny
      w=2*pi*f;
      dod=1;
      x = zeros(1,length(t));
      for x2=1:2:accuracy1;
          if dod==1
              dod=0;
              x=x+((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2)); 
          else
              dod=1;
              x=x-((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2));
          end
      end
       end
       %---------------------------------------%
       function [ x ] = getSineSignal(obj,A,f,sim_time,fi,t) % Metoda zwracaj�ca pr�bk� w czasie
       x=A*sin(2*pi*t*f+fi);
       end
       %---------------------------------------%
       function [ x1x2 ] = addSignal(obj,x1,x2) % Metoda zwracaj�ca sume pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1+x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1+x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1+x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       function [ x1x2 ] = subSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1-x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1-x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1-x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
            %---------------------------------------%
       function [ x1x2 ] = multSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1.*x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1.*x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1.*x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
        %---------------------------------------%
       function [ x1x2 ] = divSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1./x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1./x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1./x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
              %---------------------------------------%
       function [ MSE ] = getMSE(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
       MSE=immse(x1,x2);    
       end
       %---------------------------------------%
       function [ x1d ] = getDecimation(obj,x1,M)
       N=max(size(x1));
       x1d=x1(1:M:N)
       end
       %---------------------------------------%
       function [ A ] = doDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
              A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(-k*n);
              %A(k)=A(k)+x(n)*exp((-i)*2*pi*k*n/N);
           end
       end
       end
     %---------------------------------------%
      function [ A ] = doIDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
               %A(k)=A(k)+x(n)*exp(i*2*pi*k*n/N);
               A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(k*n);
           end
       end
       A=A./N;
       end
     %---------------------------------------%
   end    % -----> merody w klasie <----
   
end % -----> Koniec klasy <----