clear all
close all
clc
%-- LAB 01 --
%getSineSignal
%-- LAB 02 ---
%getSquareSignal
%getSawToothSignal
%getTriangleSignal
%-- LAB 03 --
%multSignal
%divSignal
%addSignal
%subSignal
%getMSE
%-- LAB 04 --
%getDecimation
%-- LAB 05 --
%doDFT
%doIDFT
%-- LAB 06 --
fs = 1024;%Hz
A=1;%amplituda
f=10;%Hz
fi=0;%rad
sim_time=1;%czas symulacji
t=0:1/1024:sim_time;%ilosc probek
accuracy1=4;
accuracy1=accuracy1*2-1;
ST = pt(fs);
%getSineSignal
%getSineSignal(obj,A,f,sim_time,fi,t)
%[ x1 ] = ST.getSineSignal(A,f,sim_time,fi,t); 
[ x1 ] = ST.getSineSignal(A,f,sim_time,fi,t);  
[ d1 ] = ST.doDFT(x1);
[ d2 ] = ST.doIDFT(d1);
%figure('Name','Sinusoida');
[ d3 ] = ST.getDecimation(x1,20);
subplot(2,1,1);
plot(x1);
subplot(2,1,2);
plot(d3);
% %------------------------------%
% subplot(3,1,1);
% plot(t,abs(d1),'r');
% grid on
% title('|DFT| A=1, f=10Hz, fi=0, time=1s');
% xlabel('Czas [s]'), ylabel('Amplituda');
% %------------------------------%
% subplot(3,1,2);
% plot(t,d2,'b')
% grid on
% title('IDFT A=1, f=10Hz, fi=0, time=1s');
% xlabel('Czas [s]'), ylabel('Amplituda');
% %------------------------------%
% subplot(3,1,3);
% plot(t,x1,'b')
% grid on
% title('Square (H=4) A=1, f=10Hz, fi=0, time=1s');
% xlabel('Czas [s]'), ylabel('Amplituda');



