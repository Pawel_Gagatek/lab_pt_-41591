classdef pt % -----> Deklaracja klasy pt <----
   %PT - Podstawy teleinformatyki
   % Gagatek Pawe� 41591
   properties
      internal_fs %zmienna wewn�trz klasy 
   end
   
   methods  % -----> merody w klasie <----
      function [ obj ] = pt( fs )
         %konstruktor ( sprawdza dane wejsciowe )
         if nargin > 0 % liczba argumen�w wejsciowych 
            if isnumeric(fs)
               obj.internal_fs = fs;
            else
               error('Bad input data')
            end
         end
      end
       %---------------------------------------%
      function [ x ] = getSquareSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal prostokatny
      w=2*pi*f;
      x = zeros(1,length(t));
      for i=1:2:accuracy1;
      x=x+((4*A)/pi)*((sin(i*w*t))/i); 
      end
      end
      %---------------------------------------%
      function [ x ] = getSawToothSignal(obj,A,f,sim_time,fi,t,accuracy2) % Metoda zwracaj�ca sygnalpi�okszta�tny
      w=2*pi*f;
      x = zeros(1,length(t));
      for i=1:1:accuracy2;
          if mod(i,2)==1
              x=x+((2*A)/pi)*((sin(i*w*t))/i); 
          else
              x=x-((2*A)/pi)*((sin(i*w*t))/i);
          end
      end
      end
       %---------------------------------------%       
       function [ x ] = getTriangleSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal trojkatny
      w=2*pi*f;
      dod=1;
      x = zeros(1,length(t));
      for i=1:2:accuracy1;
          if dod==1
              dod=0;
              x=x+((8*A)/(pi*pi))*((sin(i*w*t))/(i*i)); 
          else
              dod=1;
              x=x-((8*A)/(pi*pi))*((sin(i*w*t))/(i*i));
          end
      end
       end
       %---------------------------------------%
       function [ x ] = getSineSignal(obj,A,f,sim_time,fi,t) % Metoda zwracaj�ca pr�bk� w czasie
       x=A*sin(2*pi*t*f+fi);
       end
       %---------------------------------------%
       function [ x1x2 ] = addSignal(obj,x1,x2) % Metoda zwracaj�ca sume pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1+x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1+x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1+x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       function [ x1x2 ] = subSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1-x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1-x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1-x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
            %---------------------------------------%
       function [ x1x2 ] = multSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1.*x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1.*x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1.*x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
        %---------------------------------------%
       function [ x1x2 ] = divSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1./x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1./x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1./x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
              %---------------------------------------%
       function [ MSE ] = getMSE(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
       MSE=immse(x1,x2);    
       end
       %---------------------------------------%
       function [x1d] = getDecimation(x1, M)
       x1d=decimate(x1,M);
       end
     %---------------------------------------%
   end    % -----> merody w klasie <----
   
end % -----> Koniec klasy <----