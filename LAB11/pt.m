classdef pt < handle % -----> Deklaracja klasy pt <----
   %PT - Podstawy teleinformatyki
   % Gagatek Pawe� 41591
   properties %( Access = public )
      internal_fs; %zmienna wewn�trz klasy 
      audio_sample;
      image_sample;
      txt_file;
   end
       %%   
       methods( Access = public ) % -----> merody w klasie <----
       %%
       function [ obj ] = pt(  ) % / pt ( fs )
          obj.internal_fs = 0 ;
          audio_sample="";
          image_sample="";
          txt_file="";
%         konstruktor ( sprawdza dane wejsciowe )
%          if nargin > 0 % liczba argumen�w wejsciowych 
%             if isnumeric(fs)
%                obj.internal_fs = fs;
%             else
%                error('Bad input data')
%             end
%          end
       end
       %---------------------------------------%
       %%
       function [ x ] = getSquareSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal prostokatny
       w=2*pi*f;
       x = zeros(1,length(t));
       for x2=1:2:accuracy1
       x=x+((4*A)/pi)*((sin(x2*w*t))/x2); 
       end
       end
       %---------------------------------------%
       %%
       function [ x ] = getSawToothSignal(obj,A,f,sim_time,fi,t,accuracy2) % Metoda zwracaj�ca sygnalpi�okszta�tny
       w=2*pi*f;
       x = zeros(1,length(t));
       for x2=1:1:accuracy2;
          if mod(x2,2)==1
              x=x+((2*A)/pi)*((sin(x2*w*t))/x2); 
          else
              x=x-((2*A)/pi)*((sin(x2*w*t))/x2);
          end
       end
       end
       %---------------------------------------%       
       %%
       function [ x ] = getTriangleSignal(obj,A,f,sim_time,fi,t,accuracy1) % Metoda zwracaj�ca sygnal trojkatny
       w=2*pi*f;
       dod=1;
       x = zeros(1,length(t));
       for x2=1:2:accuracy1;
          if dod==1
              dod=0;
              x=x+((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2)); 
          else
              dod=1;
              x=x-((8*A)/(pi*pi))*((sin(x2*w*t))/(x2*x2));
          end
       end
       end
       %---------------------------------------%
       %%       
       function [ x ] = getSineSignal(obj,A,f,sim_time,fi,t) % Metoda zwracaj�ca pr�bk� w czasie
       x=A*sin(2*pi*t*f+fi);
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = addSignal(obj,x1,x2) % Metoda zwracaj�ca sume pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1+x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1+x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1+x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = subSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1-x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1-x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1-x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = multSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1.*x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1.*x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1.*x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%       
       function [ x1x2 ] = divSignal(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
        check1=isvector(x2)
        if check1==1 %je�li  jest wektorem
            check11=isrow(x2)
            if check11==1 %je�eli jest w orietacji poziomej
                x1x2=x1./x2;
            else %je�eli jest w orietacji poziomej
                x1x2=x1./x2';
            end
        
        else
        check2=isscalar(x2)
        if check2==1 %je�li jest sklarem
        x1x2=x1./x2; %ale do tego chyba i tak nie ma szans doj�� 
        end
        end
       end
       %---------------------------------------%
       %%      
       function [ MSE ] = getMSE(obj,x1,x2) % Metoda zwracaj�ca r�nice pr�bek
       MSE=immse(x1,x2);    
       end
       %---------------------------------------%
       %%      
       function [ x1d ] = getDecimation(obj,x1,M)
       N=max(size(x1));
       x1d=x1(1:M:N)
       end
       %---------------------------------------%
       %%      
       function [ A ] = doDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
              A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(-k*n);
              %A(k)=A(k)+x(n)*exp((-i)*2*pi*k*n/N);
           end
       end
       end
       %---------------------------------------%
       %%    
       function [ A ] = doIDFT(obj,x)
       N=max(size(x));
       A=zeros(1,N);
       for k=1:1:N
           for n=1:1:N
               %A(k)=A(k)+x(n)*exp(i*2*pi*k*n/N);
               A(k)=A(k)+x(n)*exp(i*((2*pi)/N))^(k*n);
           end
       end
       A=A./N;
       end
       %---------------------------------------%
       %%      
       function [ y ] = myFFT(obj,vect)
        leng=max(size(vect));
        if leng==1
             y=vect;
        else
            %even(1:1:leng/2)=vect(1:2:leng-1);
            %odd(1:1:leng/2)=vect(2:2:leng); 
            y=[obj.myFFT(vect(1:2:leng-1)) obj.myFFT(vect(2:2:leng))];
            for n=1:leng/2
                  t=y(n);
                  y(n)=t+exp(2*pi*i*n/leng)*y(n+leng/2);
                  y(n+leng/2)=t-exp(2*pi*i*n/leng)*y(n+leng/2);
            end
        end
       end
       %---------------------------------------%
       %%    
       function [ G ] = doDCT(obj,vect)
       N=max(size(vect));
       G=zeros(1,N);
       for m=1:N
            G(1)=G(1)+vect(m);
        end
        G(1)=G(1)/sqrt(N);
        for k=2:N
            for m=1:N
                G(k)=G(k)+vect(m)*cos((pi*k*(2*m)+1)/(2*N));                
            end
            G(k)=G(k)*sqrt(2/N);
        end
       end
       %---------------------------------------%
       %%    
       function [ G ] = doIDCT(obj,vect)
       N=max(size(vect));
       G=zeros(1,N);
       for k=1:N
            for m=1:N
                G(k)=G(k)+vect(m)*cos((pi*k*(2*m)+1)/(2*N));                
            end
            G(k)=G(k)+1/sqrt(N)*vect(1)+sqrt(2/N);
       end
       end
       %---------------------------------------%
       %%    
       function [ obj ] = loadAudio(obj,filename)
       obj.audio_sample=audioread(filename);
       end
       %---------------------------------------%
       %%     
       function [ obj ] = loadImage(obj,filename)
       obj.image_sample=imread(filename);
       end
       %---------------------------------------%
       %%    
       function [ obj ] = loadTxt(obj,filename)
       obj.txt_file=importdata(filename);  
       end
       %---------------------------------------%
       %%    
       function [ H ] = doEntropy(obj , sortedProbabilityDistribution );
       H=sortedProbabilityDistribution.*log2(sortedProbabilityDistribution)*(-1);
       H=sum(H);
       end
       %---------------------------------------%
       %%     
       function [ L ] = doAverageCodeLength(obj, codeBook, sortedProbabilityDistribution )
       F1 = cellfun(@length, codeBook(1:end,2)); % dlugosc bit�w na kazda litere
       F2=zeros(length(F1),1); % zamiana index�w
       for x=1:length(F1)
            F2(x)=F1(length(F1)-x+1);
       end
       L=sum(sortedProbabilityDistribution.*F2);
       end
       %---------------------------------------%
       %%     
       function [ E ] = doEfficiency( obj , H, L );
       E=H/L*100;
       end
       %---------------------------------------%
       %%
       function [ alphabet ] = doAlphabet( obj , msg )
       alphabet='';
       s=['A':'Z' 'a':'z'  '!':'?' '{':'}' ' `~']
       % Dopuszczalny s�ownik kodowy
       if isnumeric(msg)==1
           msg=sprintf('%g',msg);
           %The more compact of %e or %f, with no trailing zeros     
           %(Use a precision operator to specify the number of significant digits.)
           %Format data into string
           %g- po prostu format liczb
       end
       b=1;
       for x3=s
            a=strfind(msg,x3);
            if length(a)>0
                alphabet(b)=x3;
                b=b+1;   
            end
       end
       end
       %---------------------------------------%
       %% transformacja do chara
       function [ msg] = doTransformToChar ( obj , msg )
       if isnumeric(msg)==1
           msg=sprintf('%g',msg);
           %The more compact of %e or %f, with no trailing zeros     
           %(Use a precision operator to specify the number of significant digits.)
           %Format data into string
           %g- po prostu format liczb
       end
       end
       %---------------------------------------%
       %% losowy string
       function [ msg] = getRandomMsg ( obj , s , dlugoscKodu)
       % s=['A':'Z' 'a':'z'  '!':'?' '{':'}' ' `~']
       msg=s(randi(length(s),dlugoscKodu,1));     
       end
       %% Metody  do kompresji huffmana -- do 562 lini 
       %% code book
       function [ codeBook ] = huffTree(obj, sortedAlphabet, sortedProbabilityDistribution )



       % Rotate vector of probability distribution
       sortedProbabilityDistribution=sortedProbabilityDistribution';

       % Display character vs. probability

       disp('Character Probability:');
       for i = 1:length(sortedProbabilityDistribution)
            display(strcat(sortedAlphabet(i),' -->  ',num2str(sortedProbabilityDistribution(i))));
       end


       % A cell array is used to hold the groups of symbols for encoding. The process of huffman encoding combines characters into larger symbols, and a cell is the ideal type of container for managing this type of data in MATLAB. Initialize a cell array (where each cell is a symbol)

       for i = 1:length(sortedAlphabet)
            sorted_str{i} = sortedAlphabet(i);
       end

       % Save initial set of symbols and probabilities for later use
       init_str = sorted_str;
       init_prob = sortedProbabilityDistribution;

       % Iteratively sort and combine symbols based on their probabilities. Each iteration sorts the available symbols and then takes the two symbols with the "worst" probabilties and combines them into a single symbol for the next iteration. The loop halts when only a single symbol remains.

       sorted_prob = sortedProbabilityDistribution;
       rear = 1;
       while (length(sorted_prob) > 1)
            % Sort probs
            [sorted_prob,indeces] = sort(sorted_prob,'ascend');
            % Sort string based on indeces
            sorted_str = sorted_str(indeces);

            % Create new symbol
            new_node = strcat(sorted_str(1),sorted_str(2));
            new_prob = sum(sorted_prob(1:2));



            % Dequeue used symbols from "old" queue
            sorted_str =  sorted_str(3:length(sorted_str));
            sorted_prob = sorted_prob(3:length(sorted_prob));


            % Add new symbol back to "old" queue

            sorted_str = [sorted_str, new_node];
            sorted_prob = [sorted_prob, new_prob];

            % Add new symbol to "new" queue
            newq_str(rear) = new_node;
            newq_prob(rear) = new_prob;
            rear = rear + 1;
       end


       % Get all elements for the tree (symbols and probabilities) by concatenating the original set of symbols with the new combined symbols and probabilities

       tree = [newq_str,init_str];
       tree_prob = [newq_prob, init_prob];

       % Sort all tree elements
       [sorted_tree_prob,indeces] = sort(tree_prob,'descend');
       sorted_tree = tree(indeces);

       % Calculate parent relationships for all tree elements. This will allow the treeplot command to realize the correct tree structure.

       parent(1) = 0;
       for i = 2:length(sorted_tree)
           % Extract my symbol
           me = sorted_tree{i};

           % Find my parent's symbol (search until shortest match is found)
           count = 1;
           parent_maybe = sorted_tree{i-count};
           diff = strfind(parent_maybe,me);
           while (isempty(diff))
                count = count + 1;
                parent_maybe = sorted_tree{i-count};
                diff = strfind(parent_maybe,me);
           end
           parent(i) = i - count;
       end

       figure
       treeplot(parent);
       title(strcat('Huffman Coding Tree - "',sortedAlphabet,'"'));


       % Print out tree in text form
       display(sorted_tree)
       display(sorted_tree_prob)


       % Extract binary tree parameters. These parameters include the (x,y) coordinates of each node so that we can accurately place label on the tree.

       [xs,ys,h,s] = treelayout(parent);


       % Place labels on each tree node

       text(xs,ys,sorted_tree);


       % Put weights on the tree. The slope of each edge indicates whether it is a left-branch or a right-branch. Left-branches have positive slope and are labelled with a '1', while right-branches have negative slope and are labelled with a '0'. The labels for the weights are placed at the midpoint of each edge. Calculate mid-points for each node

       for i = 2:length(sorted_tree)
            % Get my coordinate
            my_x = xs(i);
            my_y = ys(i);

            % Get parent coordinate
            parent_x = xs(parent(i));
            parent_y = ys(parent(i));

            % Calculate weight coordinate (midpoint)
            mid_x = (my_x + parent_x)/2;
            mid_y = (my_y + parent_y)/2;

            % Calculate weight (positive slope = 1, negative = 0)
            slope  = (parent_y - my_y)/(parent_x - my_x);
            if (slope > 0)
                weight(i) = 1;
            else
                weight(i) = 0;
            end
            text(mid_x,mid_y,num2str(weight(i)));
       end


       % A huffman codebook can be calculated by traversing the huffman tree while recording the weights encountered while travelling to each node. Calculate codebook

       for i = 1:length(sorted_tree)
           % Initialize code
           code{i} = '';

           % Loop until root is found
           index = i;
           p = parent(index);
           while(p ~= 0)
                % Turn weight into code symbol
                w = num2str(weight(index));

                % Concatenate code symbol
                code{i} = strcat(w,code{i});

                % Continue towards root
                index = parent(index);
                p = parent(index);
           end
       end


        codeBook = [sorted_tree', code']


       rear=1;
       codeBookNonPrefix=cell(1,2);
       for i = 1:length(sorted_tree)
            if length(codeBook{i,1})==1
                codeBookNonPrefix{rear,1}=codeBook{i,1};
                codeBookNonPrefix{rear,2}=codeBook{i,2};
                rear=rear+1;
            end
       end
       codeBook = codeBookNonPrefix;
       end
       %---------------------------------------%
       %% do huffmana ( kodowanie )
       function [ encodeMsg ] = encodeHuff( obj , codeBook, msg )
       encodeMsg='';
       for n=1:length(msg) % leci po kazdym znaku w code booku
           tmpIdx = strfind(codeBook(1:end,1), msg(n)); % temp inx = znajdz stringa w codebooku od msg (n)
           Idx = find(not(cellfun('isempty', tmpIdx)))'; % znajduje index na ktorym znaleciono ta wartosc
           encodeMsg = [encodeMsg, [codeBook{Idx,2}]]; % wiadomosc = waidomo�� + warto�� codebooka od znalezionych indexow
       end
       end
       %---------------------------------------%
       %% do huffmana ( prawdopodobienstwo )
       function [sortedAlphabet,sortedProbabilityDistribution] = getProb( obj, data, alphabet )
       N=max(size(data));
       sortedProbabilityDistribution=zeros(max(size(alphabet)),1);
       i=1;
       for c=alphabet
           sortedProbabilityDistribution(i)=length(find(data==c))/N;
           i=i+1;
       end
       [sortedProbabilityDistribution,idx] = sort(sortedProbabilityDistribution,'ascend');
       sortedAlphabet=alphabet(idx);
       end
       %---------------------------------------%
       %%  do huffmana (dekodowanie )
       function [ msg ] = decodeHuff( obj , codeBook, encodeMsg )
       mL=0;
       for i=1:size(codeBook,1)
        mtL=length([codeBook{i,2}]);
        if mL < mtL
        mL = mtL;
        end
       end
       codeBook = sortrows(codeBook,[2]);
    
       msg='';
       N=max(size(encodeMsg));
       n=1;
       for l=1:N
        tmpCB={};
        rear=1;
        newTmpCB=codeBook;
        for ml=1:mL;
            for i=1:size(newTmpCB,1)
                if N>=(n+ml-1)
                    nml1=n+ml-1;
                    b=encodeMsg(nml1);
                    if([newTmpCB{i,2}(ml)]==b)
                        tmpCB{rear,1}=newTmpCB{i,1};
                        tmpCB{rear,2}=newTmpCB{i,2};
                        rear=rear+1;
                    end
                end
            end
        if rear==2
            msg=[msg,[tmpCB{1,1}]];
            n=n+ml;
            break;
        end
        newTmpCB=tmpCB;
        tmpCB=[];
        rear=1;
       end
       if n>N % or n>=N :)
        break;
       end
       end
       end
       %---------------------------------------%
       %% do huffmana ( rysowanie )
       function [] = plotProb( obj , sortedAlphabet, sortedProbabilityDistribution )
       labels=cell(size(sortedAlphabet));
       for i=1:max(size(sortedAlphabet))
        labels{i}=[sortedAlphabet(i)];
       end
       figure;
       bar(sortedProbabilityDistribution);
       set(gca,'xticklabel',labels);
       title('Probability of alphabet');
       xlabel('Character');
       ylabel('Probability');
       end
       %---------------------------------------%
       %% wyciagniecie skladowych RGB
       function [R, G, B] = getRGB( obj , excerpt )
       R = excerpt(:,:,1);
       G = excerpt(:,:,2);
       B = excerpt(:,:,3);
       end
       %---------------------------------------%
       %% 
       function [ excerpt ] = getExcerpt( obj , R , G , B )
       excerpt(:,:,1)=R;
       excerpt(:,:,2)=G;
       excerpt(:,:,3)=B;
       end
       %---------------------------------------%
       %% RGB to YUV
       function [Y, U, V] = rgb2yuv( obj , R, G, B )
       Y=( 0.30 * double( R ) ) + ...
         ( 0.59 * double( G ) ) + ...
         ( 0.11 * double( B ) ); 
       U=( 0.56 * ( double( B ) - Y ) ) + 128; 
       V=( 0.71 * ( double( R ) - Y ) ) + 128; 
       end
       %---------------------------------------%
       %% YUV to RGB
       function [ R , G , B ] = yuv2rgb( obj , Y, U, V )  
       B=((U-128)/(0.56))+Y;
       R=((V-128)/(0.71))+Y;
       G=(Y-(0.11*B)-(0.30*R))/(0.59);
       R=uint8(R);
       G=uint8(G);
       B=uint8(B);
       end
       %---------------------------------------%
       %% mapowanie wartosci
       function [ Y, U, V ] = mapValue( obj , Y, U, V );
       N=length(Y);
       for x=1:1:N
           for y=1:1:N
                   Y(x,y)=Y(x,y)-128;
                   U(x,y)=U(x,y)-128;
                   V(x,y)=V(x,y)-128;
           end
       end
       end
       %---------------------------------------%
       %%
       function  [ Y, U, V ] =deMapValue( obj , Y, U, V );
       N=length(Y);
       for x=1:1:N
           for y=1:1:N
                   Y(x,y)=Y(x,y)+128;
                   U(x,y)=U(x,y)+128;
                   V(x,y)=V(x,y)+128;
           end
       end
       end
       %---------------------------------------%
       %% fdct2
       function  [ F ] = YUV2FDCT( obj ,f  )
       N=8;
       F=zeros(N,N);
       for u=1:1:N
           for v=1:1:N
               uO = (( u - 1 ) * pi ) ;
               vO = (( v - 1 ) * pi ) ;
               temp=0;
               for x=1:1:N
                   for y=1:1:N
                          n1 = ( 2 *( x - 1 ) + 1 ) ;
                          n2 = ( 2 *( y - 1 ) + 1 ) ;
                          
                          s1 = ( n1 * uO );
                          s2 = ( n2 * vO );
                          
                          temp =temp +  f ( x , y ) ...
                          * cos( s1/16) ...
                          * cos( s2/16 ) ;
                   end
               end

               F(u,v)=(1/4)*obj.getC(u-1)*obj.getC(v-1)*temp;
            end   
       end
       end 
       %---------------------------------------%
       %%       
       function  [ f ] = YUV2IFDCT( obj ,F  )
       N=8;
       f=zeros(N,N);
       for x=1:1:N
           for y=1:1:N
               n1 = ( 2 *( x - 1 ) + 1 ) ;
               n2 = ( 2 *( y - 1 ) + 1 ) ;
               temp=0;
               for u=1:1:N
                   for v=1:1:N                         
                          uO = (( u - 1 ) * pi ) ;
                          vO = (( v - 1 ) * pi ) ;
                          
                          s1 = ( n1 * uO );
                          s2 = ( n2 * vO );
                          
                          temp =temp +  F ( u , v ) ...
                          * obj.getC(u-1)*obj.getC(v-1) ...
                          * cos( s1/16) ...
                          * cos( s2/16 ) ;
                   end
               end

               f(x,y)=(1/4)*temp;
            end   
       end
       end 
       %---------------------------------------%
       %% pomo4niczy do dct2
       function [ C ] = getC( obj , N )
            if N==0
                C=1/sqrt(2);
            else
                C=1;
            end
       end
       %---------------------------------------%
       %% kwantyzacja
       function [ qY, qU, qV ] = fYUV2qYUV( obj , fY, fU, fV, Qy, Quv, c );
       for x=1:1:8
           for y=1:1:8
           Qy(x,y)=Qy(x,y)*c;
           Quv(x,y)=Quv(x,y)*c;
           qY(x,y)=round(fY(x,y)/Qy(x,y));
           qU(x,y)=round(fU(x,y)/Quv(x,y));
           qV(x,y)=round(fV(x,y)/Quv(x,y));
           end
       end
       end
       %---------------------------------------%
       %%
       %% DE kwantyzacja
       function [ fY, fU, fV ] = qYUV2fYUV ( obj ,qY, qU, qV, Qy, Quv, c );
       for x=1:1:8
           for y=1:1:8
             % tablice ze stopniem kompresji
            Qy(x,y)=Qy(x,y)*c;
            Quv(x,y)=Quv(x,y)*c;

              % de kwantyzacja
            fY(x,y)=round(qY(x,y)*Qy(x,y));
            fU(x,y)=round(qU(x,y)*Quv(x,y));
            fV(x,y)=round(qV(x,y)*Quv(x,y));
           end
       end
       end
       %---------------------------------------%
       %% ZigZag 
       function [ zY ]= qYUV2zYUV ( obj , qY )
           idx =[1 9 2 3 10 17 25 18 ...
                 11  4 5 12 19 26 33 41 ...
                 34 27 20 13 6 7 14 21 ...
                 28 35 42 49 57 50 43 36 ...
                 29 22 15 8 16 23  30 37 ...
                 44 51 58 59 52 45 38 31 ...
                 24 32 39 46 53 60 61 54 ...
                 47 40 48 55 62 63 56 64];
             zY=qY(idx);
       end
              %---------------------------------------%
       %% iZigZag 
       function [ qY ]= zYUV2qYUV ( obj , zY )
           idx =[1 9 2 3 10 17 25 18 ...
                 11  4 5 12 19 26 33 41 ...
                 34 27 20 13 6 7 14 21 ...
                 28 35 42 49 57 50 43 36 ...
                 29 22 15 8 16 23  30 37 ...
                 44 51 58 59 52 45 38 31 ...
                 24 32 39 46 53 60 61 54 ...
                 47 40 48 55 62 63 56 64];
                qY=double(zeros(8,8));
                qY(idx)=zY;
       end
       %---------------------------------------%
       %% Znaki na bez znak�w
       function [ z ] = sign2unsign( obj , z )
       n=length(z);
       z=z*2;
       for x=1:1:n
           if z(x)<0
                      z(x)=z(x)*(-1);
                      if mod(z(x),2)== 0
                      z(x)=z(x)+1;
                      end
           else
                      if mod(z(x),2)==1               
                      z(x)=z(x)+1;   
                      end
           end
       end
       end
       %---------------------------------------%
       %% bez znak�w na znaki
       function [ z ] = unsign2sign( obj , z )
       n=length(z);
       for x=1:1:n
           if mod(z(x),2)==1
           z(x)=(z(x)-1)*(-1);          
           end
       end
       z=z/2;
       end
       %---------------------------------------%      
       %% rle dla string�w ( moje )
       function  [ r ] = uYUV2RLE( obj , u )
       n=length(u);
       r='';
       x=1;
       while x<n+1
           if x==n
               r=strcat(r,u(x),'1');
           else
               if ( u(x) ~= u(x+1))
                    r=strcat(r,u(x),'1');    
               else
                   %%
                   d=x+1;
                   d2=2;       
                   while  u(x)== u(d+1)
                       d2=d2+1;
                       d=d+1; 
                       if  (d+1)>n
                           break;
                       end
                   end   
                   d3=int2str(d2);
                   r=strcat(r,u(x),d3); 
                   x=d;
               end
           end
           x=x+1;
       end
       end       
       %---------------------------------------%
       %% RLE ( z gita )
       function [ rv ] = RLE64( obj , v )
       % rv - edcoded data (signed int)
       % v  - decoded data (must be unsigned int)
       % By Cezary Wernik
           D=0;
           U=1;
           idx=[];
           ix=1;
           idx(ix)=0;
           for i=2:64
              if v(i-1)==v(i)
                 if U==1 && ix>1
                    idx(ix)=D+1;
                    ix=ix+1;
                 elseif U==1
                    idx(ix)=D;
                    ix=ix+1;
                 end
                 U=U+1;
                 D=0;
                 if i==64
                    idx(ix)=U;
                 end
              else
                 if D==0 && i>2
                    idx(ix)=U;
                    ix=ix+1;
                 end
                 U=1;
                 D=D-1;
                 if i==64
                    idx(ix)=D;
                 end
              end
           end
           % scrolling idx, reducing zeros and values  repeating less than 3 times
           idxD=[];
           idxD(1)=0;
           ixD=1;
           for i=1:ix
              if idx(i)<3
                 idx(i);
                 idxD(ixD)=-abs(idx(i))+idxD(ixD);
              else
                 ixD=ixD+1;
                 idxD(ixD)=idx(i);
                 if i<ix
                    ixD=ixD+1;
                    idxD(ixD)=0;
                 end
              end
           end
           % rewriting output data after computed offsets
           aI=1;
           vI=1;
           rv(aI)=int8(0);
           for i=1:length(idxD)
              rv(aI)=int8(idxD(i));
              if idxD(i)<0
                 rv(aI+1:aI+abs(idxD(i)))=int8(v(vI:vI+abs(idxD(i))-1));
                 aI=aI+abs(idxD(i))+1;
                 vI=vI+abs(idxD(i));
              else
                 rv(aI+1)=int8(v(vI));
                 aI=aI+2;
                 vI=vI+abs(idxD(i));
              end   
           end
        end
       %---------------------------------------%
       %% IRLE ( z gita )
       function [ v ] = IRLE64( obj , rv )
       % rv - edcoded data (signed int)
       % v  - decoded data (should be unsigned int)
       % By Cezary Wernik
            N=length(rv);
            n=1;

            I=1;
            v(I)=int8(0);

            while 1
               if rv(n)<0
                  % unique reading
                  for k=n+1:n+abs(rv(n))
                     v(I)=rv(k);
                     I=I+1;
                  end
                  n=n+abs(rv(n))+1;
               else
                  % repeatable reading
                  for k=1:rv(n)
                     v(I)=rv(n+1);
                     I=I+1;
                  end
                  n=n+1+1;
               end
               if n>=N;
                  break;
               end
            end
        end
       %---------------------------------------%
       %%
       function [h] = h74_encode( obj , data )
       G=[ 1 1 0 1;
           1 0 1 1;
           1 0 0 0;
           0 1 1 1;
           0 1 0 0;
           0 0 1 0;
           0 0 0 1 ];
       h=G*data;
       h=h';
       h=mod(h,2);
       end
       %---------------------------------------%
       %%
       function [encode_data] = spoilBit( obj , idx , encode_data )
       len=length(encode_data);
       if idx==0 % losowy bit
            %losuj 
            %index & zeruj
            los=randi(len,1,1);
            encode_data(los)=~(encode_data(los));
       else % wybrany bit
            encode_data(idx)=~(encode_data(idx));
       end
       end
       %---------------------------------------%
       %%
       function [ d , idx ] = h74_decode( obj , h )
       H=[ 1 0 1 0 1 0 1;
            0 1 1 0 0 1 1;
            0 0 0 1 1 1 1];
       p=H*h';
       p=mod(p,2);
       idx=p(1)+p(2)*2+p(3)*4;
       if idx==0
            d=h([3 5 6 7]);
       else
            h(idx)=~(h(idx));
            d=h([3 5 6 7]);
       end
       end
       %---------------------------------------%
       %% dla 4 bit�w parzysto�ci 
       function [h] = h84_encode( obj , data )
       h=[obj.h74_encode(data) mod(sum(obj.h74_encode(data)),2)];
       end
       %---------------------------------------%
       function [encode_data] = spoilBit2( obj , idx , idx2 , encode_data )
       if idx==0 % losowy bit
                los=randi(8,1,1);
                encode_data(los)=~(encode_data(los));
                if idx2==0 % losowy bit
                    los2=randi(8,1,1);
                    while los==los2
                        los2=randi(8,1,1);
                    end
                    encode_data(los2)=~(encode_data(los2));
                else
                    encode_data(idx2)=~(encode_data(idx2));
                end
       else % wybrany bit
            encode_data(idx)=~(encode_data(idx));
            if idx2==0 % losowy bit
                    los2=randi(8,1,1);
                    while los2==idx
                        los2=randi(8,1,1);
                    end
                    encode_data(los2)=~(encode_data(los2));
            else %wybrany bit
                    if idx==idx2
                        disp("Podales 2 te same indexy wi�c drugi wylosowano.");
                        los2=randi(8,1,1);
                        while los2==idx
                            los2=randi(8,1,1);
                        end
                        encode_data(los2)=~(encode_data(los2));
                    else
                    encode_data(idx2)=~(encode_data(idx2));
                    end
            end
       end
       end
       %---------------------------------------%
       %%
       function [ d , idx ] = h84_decode( obj , h )
       end
       %---------------------------------------%
       %%
       function [ d ] = ttlGenerator( obj , CLK , data )
       leng=length(CLK);
       leng2=length(data);
       d=zeros(1,leng);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=100;
               d(down:top)= data(x);
               
           else
               down=(x-1)*100;
               top=(x-1)*100+100;
               d(down:1:top)=data(x);
           end
           
       end
       end
       %---------------------------------------%
       %%
       function [ d ] = manchesterGenerator( obj , CLK , data ); 
       % -1 to 0  :)
       leng=length(CLK);
       leng2=length(data);
       d=zeros(1,leng);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=100;
               if(data(x)==0)
                   d(down:down+50)=-1;
                   d(down+50:top)=1;
               else
                   d(down:down+50)=1;
                   d(down+50:top)=-1;
               end                         
           else
               down=(x-1)*100;
               top=(x-1)*100+100;
               if(data(x)==0)
                   d(down:down+50)=-1;
                   d(down+50:top)=1;
               else
                   d(down:down+50)=1;
                   d(down+50:top)=-1;
               end   
           end          
       end
       d(1:50)=0;
       end 
       %---------------------------------------%
       %%
       function [ d ] = nrziGenerator( ~ , CLK , data )
       % -1 to 0  :)
       leng=length(CLK);
       leng2=length(data);
       d=zeros(1,leng);
       d2=zeros(1,leng);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=100;
               if(data(x)==0)
                   d(down:top)=1;
               else
                   d(down:top)=-1;
               end                         
           else
               down=(x-1)*100;
               top=(x-1)*100+100;
               if(data(x)==0)
                   d(down:top)=d(down-1);
               else
                   d(down:top)=d(down-1)*-1;
               end   
           end          
       end
       % bez tego jest innne ale nie przesuni�te o 50 .. ;(
       for x=51:1:leng-50
           d2(x)=d(x-50);
       end
       d2(leng-50:leng)=d(leng-50:leng);
       d=[d2];
       d(1:50)=0;
       end 
       %---------------------------------------% 
       %%
       function [ d ] = bamiGenerator(obj, CLK , data);
       % -1 to 0  :)
       leng=length(CLK);
       leng2=length(data);
       d=zeros(1,leng);
       check=1;
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=100;
               if(data(x)==0)
                   d(down:top)=0;
               else
                   if(check==1)
                       d(down:top)=1;
                       check=0;
                   else
                       d(down:top)=-1;
                       check=1;
                   end
                   
               end                         
           else
               down=(x-1)*100;
               top=(x-1)*100+100;
               if(data(x)==0)
                   d(down:top)=0;
               else
                   if(check==1)
                       d(down:top)=1;
                       check=0;
                   else
                       d(down:top)=-1;
                       check=1;
                   end
                   
               end    
           end          
       end
       d(1:100)=0;
       end 
       %---------------------------------------% 
       %%
       function [ d ] = manDecode( obj ,MAN )
       % -1 to 0  :)
       leng=length(MAN);
       leng=leng/100;
       d=zeros(1,leng);
       for x=1:1:leng
           if(x==1)
               down=1;
               top=100;
               if ( MAN(top-1)==1)
                   d(x)=0;
               else
                   d(x)=1;
               end                         
           else
               down=(x-1)*100;
               top=(x-1)*100+100;
               if(MAN(down+1)==-1 & MAN(top-1)==1)
                   d(x)=0;
               else
                   d(x)=1;
               end   
           end          
       end
       end
       %---------------------------------------% 
       %%
       function [ d ] = B8ZS(obj, CLK , data)
       % -1 to 0  :)
       leng=length(CLK);
       leng2=length(data);
       d=zeros(1,leng);
       check=1;
       x=1;
       while leng~=x
       if(x==leng2+1) 
           break
       end
       %for x=1:1:leng2
           disp(x);
            if(x==1)
               down=1;
               top=100;
               if(data(x)==0)
                   d(down:top)=0;
               else
                   if(check==1)
                       d(down:top)=1;
                       check=0;
                   else
                       d(down:top)=-1;
                       check=1;
                   end
                   
               end                         
           else
              down=(x-1)*100;
              top=(x-1)*100+100;
              if(data(x)==0)
                   if(x+7<=leng2 & data(x:1:x+7)==0)
                       disp("8 zer ");
                       d(down:top)=0;
                       down=down+100;
                       top=top+100;
                       d(down:top)=0;
                       down=down+100;
                       top=top+100;
                       d(down:top)=0;
                       down=down+100;
                       top=top+100;
                       if check==0
                           d(down:top)=1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=-1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=0;
                           down=down+100;
                           top=top+100;
                           d(down:top)=-1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=1;                       
                           check=0;
                       else
                           d(down:top)=-1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=0;
                           down=down+100;
                           top=top+100;
                           d(down:top)=1;
                           down=down+100;
                           top=top+100;
                           d(down:top)=-1;
                           check=1;
                       end
                       x=x+7;
                   else
                        d(down:top)=0;
                   end
               else
                   if(check==1)
                       d(down:top)=1;
                       check=0;
                   else
                       d(down:top)=-1;
                       check=1;
                   end
                   
               end   
            end  
       x=x+1;
       end
       end
       %---------------------------------------% 
       function [ d ] = askGenerator(obj,CLK,data)
       r1=length(data);
       r2=length(CLK);
       d=zeros(1,r2);
       klaster=r2/r1;
       dod=(obj.getSineSignal(1,1,1,0,0:1/(klaster-1):1));
       
       leng2=length(data);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=klaster;
               d(down:top)= dod;             
           else
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(data(x)==0)
                   d(down:top)=0;
               else
                   d(down:top-1)=dod;
               end
           end
       end
       end
       %%
       %---------------------------------------% 
       function [ d ] = fskGenerator(obj,CLK,data)
       r1=length(data);
       r2=length(CLK);
       d=zeros(1,r2);
       klaster=r2/r1;
       %dod=(obj.getSineSignal(A,f,sim_time,fi,t));
       dod= (obj.getSineSignal(1,1,1,0,0:1/(klaster-1):1));
       dod2=(obj.getSineSignal(1,2,1,0,0:1/(klaster-1):1));
       leng2=length(data);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=klaster;
               d(down:top)= dod;             
           else
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(data(x)==0)
                   d(down:top-1)=dod2;
               else
                   d(down:top-1)=dod;
               end
           end
       end
       end
       %%
       %---------------------------------------%
       function [ d ] = pskGenerator(obj,CLK,data)
       r1=length(data);
       r2=length(CLK);
       d=zeros(1,r2);
       klaster=r2/r1;
       %dod=(obj.getSineSignal(A,f,sim_time,fi,t));
       dod= (obj.getSineSignal(1,1,1,0,0:1/(klaster-1):1));
       dod2=dod*-1;
       leng2=length(data);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=klaster;
               d(down:top)= dod;             
           else
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(data(x)==0)
                   d(down:top-1)=dod2;
               else
                   d(down:top-1)=dod;
               end
           end
       end
       end
       %%
       %---------------------------------------%
       function [ d ] = askDecode( obj ,MAN )
       klaster=100;
       leng=length(MAN);
       leng=leng/100;
       d=zeros(1,leng);
       for x=1:1:leng
           if(x==1)
               top=klaster;
               if ( MAN(top-3)==0)
                   d(x)=0;
               else
                   d(x)=1;
               end                         
           else
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(MAN(down+3)==0)
                   d(x)=0;
               else
                   d(x)=1;
               end   
           end   
       end
       end
       %---------------------------------------%
       function [ d ] = fskDecode( obj ,MAN )
       klaster=100;
       leng=length(MAN);
       leng=leng/100;
       d=zeros(1,leng);
       for x=1:1:leng
           if(x==1)
               down=1;
               top=klaster;
               if(round(MAN(down+5))==1)
                   d(x)=0;
               else
                   d(x)=1;
               end 
           else              
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(round(MAN(down+5))==1)
                   d(x)=0;
               else
                   d(x)=1;
               end
           end   
       end
       end
       %%
       %---------------------------------------%
       function [ d ] = pskDecode( obj ,MAN )
       klaster=100;
       leng=length(MAN);
       leng=leng/100;
       d=zeros(1,leng);
       for x=1:1:leng
           if(x==1)
               down=1;
               top=klaster;
               if(round(MAN(down+9))==1)
                   d(x)=1;
               else
                   d(x)=0;
               end 
           else              
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(round(MAN(down+9))==1)
                   d(x)=1;
               else
                   d(x)=0;
               end
           end   
       end
       end
       %%
       %---------------------------------------%
       function [ d ] = qpskGenerator(obj,CLK,data)
       r1=length(data);
       r2=length(CLK);
       d=zeros(1,r2);
       klaster=r2/r1;
       %dod=(obj.getSineSignal(A,f,sim_time,fi,t));
       dod= (obj.getSineSignal(1,1,1,0,0:1/(klaster-1):1));
       t=0:1/(klaster-1):0.25;
       a=1*cos(2*pi*1*t+(pi/4));
       b=1*cos(2*pi*1*t+3*(pi/4));
       c=1*cos(2*pi*1*t+5*(pi/4));
       d=1*cos(2*pi*1*t+7*(pi/4));
       dod=[a b c d];
       dod2=dod*-1;
       leng2=length(data);
       for x=1:1:leng2
           if(x==1)
               down=1;
               top=klaster;
               d(down:top)= dod;             
           else
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(data(x)==0)
                   d(down:top-1)=dod2;
               else
                   d(down:top-1)=dod;
               end
           end
       end
       d(r2)=0;
       end
              %%
       %---------------------------------------%
       function [ d ] = qpskDecode( obj ,MAN )
       klaster=100;
       leng=length(MAN);
       leng=leng/100;
       d=zeros(1,leng);
       for x=1:1:leng
           if(x==1)
               down=1;
               top=klaster;
               if(round(MAN(down+4))==1)
                   d(x)=1;
               else
                   d(x)=0;
               end 
           else              
               down=(x-1)*klaster;
               top=(x-1)*klaster+klaster;
               if(round(MAN(down+4))==1)
                   d(x)=1;
               else
                   d(x)=0;
               end
           end   
       end
       end
       %---------------------------------------%
       %%
       end    % -----> merody w klasie <----
   
end % -----> Koniec klasy <----