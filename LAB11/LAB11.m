close all
clear all
clc
ST=pt();
%% dane do ustawienia
%do testow dane pozostaw pocz�tkowo jak w oryginale
%nast�pnie zmie� dane na swj nr album zapisany binarnie
data = [1 0 1 0 1 1 0 0];%strumie� danych

%Bod (ang. Baud), typowe warto�ci: 1200/2400/4800/9600
bod=10;

%% zmienne pomocnicze
sim_fs=bod*100; %cz�stotliwo�c probkowania dla symulacji 100 wi�ksza od Bod
sp_clk=sim_fs/bod; %sampli na cykl zegara
N = length(data); %ilo�� bitow w danych wej�ciowych
sp_d=N*sp_clk; %sampli na ilo�� danych
t = 0:(1/sim_fs):((sp_d/sim_fs)-(1/sim_fs)); %czas trwania sygnalu
empty = zeros(1,length(t));%pomocniczy pusty wektor 

CLK     = clkGenerator(sp_clk,sp_d);%sygnal zegara

%% zadanie wa�ciwe
%strumie� po modulacji
%gdy napiszesz wasn� funkcj� modulatora zamie� empty na wa�ciw� nazw� funkcji
TTL     = empty;%ttlGenerator(CLK,data);
ASK     = empty;%askGenerator(TTL,bod,sim_fs);
FSK     = empty;%fskGenerator(TTL,bod*2,bod,sim_fs);
PSK     = empty;%pskGenerator(TTL,bod,sim_fs);%dok�adnie BPSK

TTL     = ST.ttlGenerator(CLK,data); 
ASK     = ST.askGenerator(CLK,data);
FSK     = ST.fskGenerator(CLK,data);
PSK     = ST.pskGenerator(CLK,data);
QPSK    = ST.qpskGenerator(CLK,data); 
%% pomocnicza funkcja rysuj�ca
%plotSignalModulations(t, CLK, TTL, ASK, FSK, PSK);
%%  dla QPSK
decodeQPSK=ST.qpskDecode(QPSK);
decodeQPSK=ST.ttlGenerator(CLK,decodeQPSK);
plotSignalModulations(t, CLK, TTL, decodeQPSK, QPSK, PSK);
%%

decodeASK=ST.askDecode(ASK);
decodeFSK=ST.fskDecode(FSK);
decodePSK=ST.pskDecode(PSK);

decodeASK=ST.ttlGenerator(CLK,decodeASK); 
decodeFSK=ST.ttlGenerator(CLK,decodeFSK); 
decodePSK=ST.ttlGenerator(CLK,decodePSK); 

plotSignalModulations(t, CLK, TTL, decodeASK, decodeFSK, decodePSK);