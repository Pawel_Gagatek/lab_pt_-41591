close all
clear all
clc

ST=pt();
%wygenerowanie danych testowych (wektor pionowy)
disp('Kod Hamminga SECDED');disp(' ');

data=[1 0 1 1]';
disp(num2str(data')+" Dane Wejsciowe");disp(' ');

disp("p1 p2 d1 p3 d2 d3 d4 p4");
encode_data=ST.h84_encode(data);
disp(num2str(encode_data)+" Dane Zakodowane");

%celowe uszkodzenie 
%encode_data=ST.spoilBit2(1,2,encode_data);
encode_data=ST.spoilBit(2,encode_data);

disp(num2str(encode_data)+" Dane uszkodzone");disp(' ');



% sprawdzenie ile bit�w uleg�o uszkedzeniu
if mod(sum(encode_data(1:7)),2)==encode_data(8)
    [decode_data,idx]=ST.h74_decode(encode_data(1:7));
    if(idx==0) 
        disp("Brak b��d�w w transmisji");
    else
        disp("Wykryto przynajmniej dwa bledy przy dekodowaniu");
    end
    
else
    %naprawa uszkodzonego bitu
    [decode_data,idx]=ST.h74_decode(encode_data(1:7));
    disp("Uszkodzony zosta� "+idx+" bit");disp(' ');
    disp('por�wnanie danych zrekonstruowanych i wej�ciowych');disp(' ');
    disp("zrekonstruowane");
    disp(decode_data);
    disp("Orginalne");
    disp(data');
 end





