close all
clear all
clc

ST=pt();
load('encodedata.mat')
load('alldata.mat')
datasize=size(encode_data_full);
decode_data_full=double([]);

for x=1:datasize(2)
    encode_data=encode_data_full([1:8 ],x);
     encode_data=encode_data';
     
    if mod(sum(encode_data(1:7)),2)==encode_data(8) % sprawdza czy
        [decode_data,idx]=ST.h74_decode(encode_data(1:7));
        if(idx==0) 
            disp("BAJT nr : "+num2str(x)+" : BRAK b��d�w ");
            decode_data_full=[decode_data_full decode_data'];
        else
            %disp("Wykryto przynajmniej dwa bledy przy dekodowaniu");
            decode_data_full=[decode_data_full decode_data'];
            disp("BAJT nr : "+num2str(x)+" : >= 2 b��dy ");
        end
    
    else
        
        %naprawa uszkodzonego bitu
        disp("BAJT nr : "+num2str(x)+" : JEDEN b��d ");
        [decode_data,idx]=ST.h74_decode(encode_data(1:7));
        decode_data_full=[decode_data_full decode_data'];
    end
end
disp(" ");
disp("Dane wej�ciowe");
disp(alldata);
disp("Dane wyj�ciowe");
disp(decode_data_full);
