close all
clear all
clc

ST=pt();
%wygenerowanie danych testowych (wektor pionowy)
disp('dane wej�ciowe');
%data=[1 0 1 0]';
data=[1 0 1 1]'

%zakodowanie kodem korekcyjnym Hamminga(7,4)
disp('dane zakodowane');
encode_data=ST.h74_encode(data)

%celowe uszkodzenie 3-ego lub losowego bitu
disp('dane uszkodzone');
encode_data=ST.spoilBit(3,encode_data)

%korekcja i odkodowanie danych
disp('dane zrekonstruowane i wskazanie kt�ry indeks by� naprawiany');
[decode_data,idx]=ST.h74_decode(encode_data)

%por�wnanie danych odkodowanych z wej�ciowymi
disp('por�wnanie danych zrekonstruowanych i wej�ciowych');
[decode_data' data]