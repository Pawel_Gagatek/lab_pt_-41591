clear all
close all
clc

fs = 1024;
ST = pt();

[lr,Fs]=audioread('everything-is-fine.wav');
subplot(5,1,1);
plot(lr);
grid on
title('Audio oryginalne');
xlabel('Czas [s]'), ylabel('Amplituda');

%Wyb�r tylko jednego kana�u
l=lr(:,1);

% Obliczenie Fs-punktowej transformaty
L=fft(l,Fs);

subplot(5,1,2);
plot(abs(L));
grid on
title('Audio po punktowej transofrmacie');
xlabel('Czas [s]'), ylabel('Amplituda');

% Odtworzenie sygna�u l, bez zerowania sk��dowych
sound(l,Fs)

% Odczekanie 1s
pause(1);

% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.93;

% Obracanie widma
L=fftshift(L);

% Obliczenie progu odci�cia i wyzerowanie kranc�w widma
r=floor((Fs/2)*q);
L(1:r)=0;
L(Fs-r :Fs)=0;

subplot(5,1,3);
plot(abs(L));
grid on
title('Audio po odcieciu ');
xlabel('Czas [s]'), ylabel('Amplituda');

% Obracanie widma do pierwotnej kolejno�ci
L=fftshift(L);

subplot(5,1,4);
plot(abs(L));
grid on
title('Audio po odcieciu i obroceniu');
xlabel('Czas [s]'), ylabel('Amplituda');

% Odwrotna transformata
l=real(ifft(L));

subplot(5,1,5);
plot(l);
grid on
title('Audio po odwrotnej transormacie');
xlabel('Czas [s]'), ylabel('Amplituda');
% Odtworzenie sygna�u l z wyzerowan� cz�ci� widma
sound(l,Fs);

