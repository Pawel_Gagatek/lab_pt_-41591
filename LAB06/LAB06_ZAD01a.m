clear all
close all
clc

ST = pt();
[l,Fs]=audioread('everything-is-fine.wav');
lr=l(:,1);
leng=Fs;
czas=linspace(0,length(lr)/Fs,length(lr)) ;
%------- wyswietlenie sygna�u d�wi�ku ------
subplot(4,1,1);
plot(lr);
grid on
title('Audio everything-is-fine.wav');
xlabel('Czas [s]'), ylabel('Amplituda');


%------- DFT na 0.2 s  ------
lewo=25500;
prawo=28000;

subplot(4,1,2);
plot(lr(lewo:1:prawo),'r');
title('Audio everything-is-fine.wav sygnal do dft');
xlabel('Czas [s]'), ylabel('Amplituda');
grid on
x3=[lr(1:1:(lewo-1))' ST.doDFT(lr(lewo:1:prawo)') lr((prawo+1):1:leng)'];
%x3=ST.doDFT(lr(lewo:1:prawo)');  % sprawdzi�
x3=fft(lr);
t=0:1/1024:czas;
subplot(4,1,3);
plot(abs(x3));
grid on
title('Audio everything-is-fine.wav po dft');
xlabel('Czas [s]'), ylabel('Amplituda');

%------- w skali decybelowej  ------
x4=10*log10(abs(x3));
%x4=mag2db(abs(x3));
subplot(4,1,4);
plot(x4);
grid on
title('Audio everything-is-fine.wav pod dft w dB');
xlabel('Czas [s]'), ylabel('dB');
% szeroko�� psama - r�nica pomi�dzy najwy�sz� a najni�sz� cz�stotliwo�ci�
% i mie�ona jest w Hz   tutaj jesy jakies 70 hz tak mi si� wydaje
