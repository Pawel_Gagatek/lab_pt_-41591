clear all
close all
clc
% Badanie sygna�u 2-D
% Odczyt pliku graficznego
xy = imread('lennagrey.png');
% Przekszta�cenie formatu RGB na 2-D odcienie szaro�ci
xy = rgb2gray(xy);
% Rysowanie

figure,imshow(xy)
grid on
title('Oryginalny obraz ');

% Obliczenie dwuwymiarowego fft
XY=fft2(xy);


figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar 
grid on
title('Obraz po fft2 ');
xlabel('Czas [s]'), ylabel('Czestotliwosc [Hz]'), zlabel('Decybele [dB]');

% Obr�t w symetrii transformaty
XY=fftshift(XY);


figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
grid on
title('fft odw�rcone w symetrii  ');
xlabel('Czas [s]'), ylabel('Czestotliwosc [Hz]'), zlabel('Decybele [dB]');




% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.1;
% Zerowanie pewnego pasma dolnych cz�stotliwo�ci
ZZ=XY;
XY(256-floor(128*q):256+floor(128*q),256-floor(128*q):256+floor(128*q))=0;


 figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
grid on
title('po odcieciu niskich cz�stotliwo�ci ');
xlabel('Czas [s]'), ylabel('Czestotliwosc [Hz]'), zlabel('Decybele [dB]');

%--------------------------
% Roboczy/pomocniczy wsp�czynnik odci�cia <0,1>
q=0.01;
gora=floor(128*q);
dol=64;
% Zerowanie pewnego pasma dolnych cz�stotliwo�ci
%ZZ(dol-gora:dol+gora,dol-gora:dol+gora)=0;
ZZ(1:250,1:250)=0;
ZZ(260:512,260:512)=0;
% obrazek jest 512 na 512

%255:257,255:257


figure,mesh(10*log10(abs(ZZ))),colormap(gca,jet(64)),colorbar
grid on
title('po odcieciu g�rnych cz�stotliwo�ci ');
xlabel('Czas [s]'), ylabel('Czestotliwosc [Hz]'), zlabel('Decybele [dB]');

%--------------------------
% Obr�t w symetrii transformaty do pierwotnej postaci
XY=fftshift(XY);


figure,mesh(10*log10(abs(XY))),colormap(gca,jet(64)),colorbar
grid on
title('Obr�t w symetrii transformaty do pierwotnej postaci ');
xlabel('Czas [s]'), ylabel('Czestotliwosc [Hz]'), zlabel('Decybele [dB]');;

% Odwrotna transformata 2-D
xy=real(ifft2(XY));


figure,imshow(xy);
grid on
title('obraz po odwrotnej transformacie 2fft ');
