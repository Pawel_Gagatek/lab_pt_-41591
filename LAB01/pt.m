classdef pt % -----> Deklaracja klasy pt <----
   %PT - Podstawy teleinformatyki
   % Gagatek Pawe� 41591
   %   przyk�ad pisania klas w kodzie matlab
   properties
      internal_fs %zmienna wewn�trz klasy 
   end
   
   methods  % -----> merody w klasie <----
      function [ obj ] = pt( fs )
         %konstruktor ( sprawdza dane wejsciowe )
         if nargin > 0 % liczba argumen�w wejsciowych 
            if isnumeric(fs)
               obj.internal_fs = fs;
            else
               error('Bad input data')
            end
         end
      end
       %---------------------------------------%
      function [ t, x ] = getSquareSignal(obj,A,f,sim_time,fi) % Metoda zwracaj�ca sygnal prostokatny
      t=0:1/1024:sim_time;
      w=2*pi*f;
      x = zeros(1,length(t));
      for i=1:2:2225;
      x=x+((4*A)/pi)*((sin(i*w*t))/i); 
      end
      end
      %---------------------------------------%
      function [ t, x ] = getSawToothSignal(obj,A,f,sim_time,fi) % Metoda zwracaj�ca sygnalpi�okszta�tny
      t=0:1/1024:sim_time;
      w=2*pi*f;
      x = zeros(1,length(t));
      for i=1:1:200;
          if mod(i,2)==1
              x=x+((2*A)/pi)*((sin(i*w*t))/i); 
          else
              x=x-((2*A)/pi)*((sin(i*w*t))/i);
          end
      end
      end
       %---------------------------------------%
       function [ t, x ] = getSineSignal(obj,A,f,sim_time,fi) % Metoda zwracaj�ca pr�bk� w czasie
       t=0:1/1024:sim_time;
       x=A*sin(2*pi*t*f+fi);
       end
        %---------------------------------------%
       function [ t, x ] = getTriangleSignal(obj,A,f,sim_time,fi) % Metoda zwracaj�ca sygnal trojkatny
      t=0:1/1024:sim_time;
      w=2*pi*f;
      dod=1;
      x = zeros(1,length(t));
      for i=1:2:4305;
          if dod==1
              dod=0;
              x=x+((8*A)/(pi*pi))*((sin(i*w*t))/(i*i)); 
          else
              dod=1;
              x=x-((8*A)/(pi*pi))*((sin(i*w*t))/(i*i));
          end
      end
      end
   end    % -----> merody w klasie <----
   
end % -----> Koniec klasy <----