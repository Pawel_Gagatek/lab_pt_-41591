clear all
close all
clc

fs = 1024;%Hz
A=1;
f=10;%Hz
fi=0;%rad
sim_time=1;
ST = pt(fs);
[ t,x ] = ST.getTriangleSignal(A,f,sim_time,fi);
figure('Name','Sinusoida');
plot(t,x,'k');
title('A=1, f=10Hz, fi=0, time=1s');
xlabel('Czas [s]'), ylabel('Amplituda');


